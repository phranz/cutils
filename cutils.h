/************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#ifndef CUTILS_H
#define CUTILS_H

#define CUTILS_VERSION "0.2.2"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#if defined(__linux__)
    #include <sys/sysmacros.h>
#else
    #include <sys/types.h>
#endif

/*
 * Equivalent to p->func(p, a, b)
 */
#define __(p, f, ...) (p->f(p, ## __VA_ARGS__))

/*
 * Allocate memory for a type (calls type's _init)
 *
 * t: type
 * ...: additional params
 */
#define alloc(t, ...) (t ## _init(malloc(sizeof(t)), ## __VA_ARGS__))


/*
 * Release memory hold by type's instance (calls ptr's free)
 *
 * p: type pointer
 */
#define release(p) (p ? (p->free(p), (p=NULL)) : NULL)

/*
 * Utility to call free and setting pointer to NULL
 *
 * p: type pointer
 */
#define zfree(p) (free(p), (p=NULL))

/*
 * Declare a vector of a specific type
 *
 * t: contained type
 * n: new type name
 */
#define dectype_vector(t, n)                 \
    typedef struct n {                       \
        t* data;                             \
        const size_t count;                  \
        void (*free)(struct n*);             \
        int (*push)(struct n*, t);           \
        int (*push_back)(struct n*, t);      \
        int (*pop)(struct n*, t*);           \
        int (*pop_back)(struct n*, t*);      \
        int (*get)(struct n*, size_t, t*);   \
        int (*first)(struct n*, t*);         \
        int (*last)(struct n*, t*);          \
        int (*put)(struct n*, size_t, t);    \
        int (*insert)(struct n*, size_t, t); \
        int (*remove)(struct n*, size_t);    \
        void (*clear)(struct n*);            \
    } n;                                     \
    n* n ## _init(n*);

#define define_vector(t, n) \
    /*
     * Add an element to the end of a vector
     *
     * p: vector pointer
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                                    \
    static int n ## _push(n* p, t v) {                     \
        if (!(p->count+1))                                 \
            return 0;                                      \
        t* mem = realloc(p->data, sizeof(t)*(p->count+1)); \
        if (mem) {                                         \
            p->data = mem;                                 \
            mem[p->count] = v;                             \
        } else {                                           \
            return 0;                                      \
        }                                                  \
        ++*(size_t*)&p->count;                             \
        return 1;                                          \
    }                                                      \
    /*
     * Add an element to the beginning of a vector
     *
     * p: vector pointer
     * v (output): a value of type t
     *
     * return: boolean operation success/failure
     */                                     \
    static int n ## _push_back(n* p, t v) { \
        return p->insert(p, 0, v);          \
    }                                       \
    /*
     * Get and remove the last element of a vector
     *
     * p: vector pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                                        \
    static int n ## _pop(n* p, t* v) {                         \
        if (!p->count || !v)                                   \
            return 0;                                          \
        if (!p->data)                                          \
            return 1;                                          \
        *v = p->data[p->count-1];                              \
        if (!(p->count-1)) {                                   \
            free(p->data);                                     \
            p->data = NULL;                                    \
        } else {                                               \
            t* mem = realloc(p->data, sizeof(t)*(p->count-1)); \
            if (mem)                                           \
                p->data = mem;                                 \
            else                                               \
                return 0;                                      \
        }                                                      \
        --*(size_t*)&p->count;                                 \
        return 1;                                              \
    }                                                          \
    /*
     * Get and remove the first element of a vector
     *
     * p: vector pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                                    \
    static int n ## _pop_back(n* p, t* v) {                \
        if (!p->count || !v)                               \
            return 0;                                      \
        *v = p->data[0];                                   \
        if (!(p->count-1)) {                               \
            free(p->data);                                 \
            p->data = NULL;                                \
            --*(size_t*)&p->count;                         \
            return 1;                                      \
        }                                                  \
        for (size_t x=0; x+1<p->count; ++x)                \
            p->data[x] = p->data[x+1];                     \
        t* mem = realloc(p->data, sizeof(t)*(p->count-1)); \
        if (mem)                                           \
            p->data = mem;                                 \
        else                                               \
            return 0;                                      \
        --*(size_t*)&p->count;                             \
        return 1;                                          \
    }                                                      \
    /*
     * Get a vector element by index
     *
     * p: vector pointer
     * i: index
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                          \
    static int n ## _get(n* p, size_t i, t* v) { \
        if (!p->count || i>p->count-1 || !v)     \
            return 0;                            \
        *v = p->data[i];                         \
        return 1;                                \
    }                                            \
    /*
     * Get the first element of a vector
     *
     * p: vector pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                  \
    static int n ## _first(n* p, t* v) { \
        if (!p->count || !v)             \
            return 0;                    \
        *v = p->data[0];                 \
        return 1;                        \
    }                                    \
    /*
     * Get the last element of a vector
     *
     * p: vector pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                 \
    static int n ## _last(n* p, t* v) { \
        if (!p->count || !v)            \
            return 0;                   \
        *v = p->data[p->count-1];       \
        return 1;                       \
    }                                   \
    /*
     * Change the value of a vector by index
     *
     * p: vector pointer
     * i: index
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                         \
    static int n ## _put(n* p, size_t i, t v) { \
        if (!p->count || i>p->count-1)          \
            return 0;                           \
        p->data[i] = v;                         \
        return 1;                               \
    }                                           \
    /*
     * Remove a vector element by index
     *
     * p: vector pointer
     * i: index
     *
     * return: boolean operation success/failure
     */                                                    \
    static int n ## _remove(n* p, size_t i) {              \
        if (!p->count)                                     \
            return 1;                                      \
        if (i>p->count-1)                                  \
            return 0;                                      \
        t l = p->data[p->count-1];                         \
        if (!(p->count-1)) {                               \
            free(p->data);                                 \
            p->data = NULL;                                \
            --*(size_t*)&p->count;                         \
            return 1;                                      \
        }                                                  \
        t* mem = realloc(p->data, sizeof(t)*(p->count-1)); \
        if (mem)                                           \
            p->data = mem;                                 \
        else                                               \
            return 0;                                      \
        --*(size_t*)&p->count;                             \
        for (size_t x=i; x+1<p->count; ++x)                \
            p->data[x] = p->data[x+1];                     \
        if (i != p->count)                                 \
            p->data[p->count-1] = l;                       \
        return 1;                                          \
    }                                                      \
    /*
     * Insert an element at index
     *
     * p: vector pointer
     * i: index
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                                    \
    static int n ## _insert(n* p, size_t i, t v) {         \
        if (i>p->count || !(p->count+1))                   \
            return 0;                                      \
        t* mem = realloc(p->data, sizeof(t)*(p->count+1)); \
        if (mem)                                           \
            p->data = mem;                                 \
        else                                               \
            return 0;                                      \
                                                           \
        if (i != p->count) {                               \
            p->data[p->count] = p->data[p->count-1];       \
            for (size_t x=p->count; x!=i; --x)             \
                p->data[x] = p->data[x-1];                 \
        }                                                  \
        p->data[i] = v;                                    \
        ++*(size_t*)&p->count;                             \
        return 1;                                          \
    }                                                      \
    /*
     * Remove all elements from a vector
     *
     * p: vector pointer
     */                             \
    static void n ## _clear(n* p) { \
        *(size_t*)&p->count = 0;    \
        free(p->data);              \
        p->data = NULL;             \
    }                               \
    /*
     * Release vector's resources
     *
     * p: vector pointer
     */                            \
    static void n ## _free(n* p) { \
        n ## _clear(p);            \
        free(p);                   \
    }                              \
    /*
     * Initialize vector's resources
     *
     * p: vector pointer
     */                                 \
    n* n ## _init(n* p) {               \
        if (!p)                         \
            return NULL;                \
        memset(p, 0, sizeof(*p));       \
        p->push = n ## _push;           \
        p->push_back = n ## _push_back; \
        p->pop = n ## _pop;             \
        p->pop_back = n ## _pop_back;   \
        p->put = n ## _put;             \
        p->get = n ## _get;             \
        p->first = n ## _first;         \
        p->last = n ## _last;           \
        p->remove = n ## _remove;       \
        p->insert = n ## _insert;       \
        p->clear = n ## _clear;         \
        p->free =  n ## _free;          \
        return p;                       \
    } 


/*
 * Declare a list of a specific type
 *
 * t: contained type
 * n: new type name
 */
#define dectype_list(t, n)         \
    typedef struct __node_ ## n {  \
        struct __node_ ## n* prev; \
        struct __node_ ## n* next; \
        t data;                    \
    } __node_ ## n;                \
    dectype_pair(size_t, __node_ ## n*, __cached_node_ ## n) \
    typedef struct n {                       \
        __node_ ## n* head;                  \
        __node_ ## n* tail;                  \
        __cached_node_ ## n cached;          \
        size_t count;                        \
        void (*free)(struct n*);             \
        int (*push)(struct n*, t);           \
        int (*push_back)(struct n*, t);      \
        int (*put)(struct n*, size_t, t);    \
        int (*insert)(struct n*, size_t, t); \
        int (*pop)(struct n*, t*);           \
        int (*pop_back)(struct n*, t*);      \
        int (*get)(struct n*, size_t, t*);   \
        int (*first)(struct n*, t*);         \
        int (*last)(struct n*, t*);          \
        int (*remove)(struct n*, size_t);    \
        void (*clear)(struct n*);            \
    } n;                                     \
    n* n ## _init(n*);

#define define_list(t, n)                          \
    static int  __ ## n ## _htalloc(n* p) {        \
        p->head = calloc(1, sizeof(__node_ ## n)); \
        if (!p->head)                              \
            return 0;                              \
        p->head->next = NULL;                      \
        p->head->prev = NULL;                      \
        p->tail = p->head;                         \
        return 1;                                  \
    }                                              \
    static int __ ## n ## _talloc(n* p) {                     \
        __node_ ## n* node = calloc(1, sizeof(__node_ ## n)); \
        if (!node)                                            \
            return 0;                                         \
        p->tail->next = node;                                 \
        node->prev = p->tail;                                 \
        node->next = NULL;                                    \
        p->tail = node;                                       \
        return 1;                                             \
    }                                                         \
    /*
     * Add an element to the end of a list
     *
     * p: list pointer
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                  \
    static int n ## _push(n* p, t v) {   \
        if (!p->head || !p->tail) {      \
            if (!__ ## n ## _htalloc(p)) \
                return 0;                \
            p->head->data = v;           \
            p->cached.val = p->head;     \
        } else {                         \
            if (!(p->count+1))           \
                return 0;                \
            if(!__ ## n ## _talloc(p))   \
                return 0;                \
            p->tail->data = v;           \
            p->cached.val = p->tail;     \
        }                                \
        ++*(size_t*)&p->count;           \
        p->cached.key = p->count-1;      \
        return 1;                        \
    }                                    \
    /*
     * Add an element to the beginning of a list
     *
     * p: list pointer
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                     \
    static int n ## _push_back(n* p, t v) { \
        return p->insert(p, 0, v);          \
    }                                       \
    /*
     * Insert an element at index
     *
     * p: list pointer
     * i: index
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                                       \
    static int n ## _insert(n* p, size_t i, t v) {            \
        if (i>p->count || !(p->count+1))                      \
            return 0;                                         \
        if (i == p->count)                                    \
            return p->push(p, v);                             \
        __node_ ## n* node = calloc(1, sizeof(__node_ ## n)); \
        if (!node)                                            \
            return 0;                                         \
        if (!i) {                                             \
            node->prev = NULL;                                \
            node->next = p->head;                             \
            p->head->prev = node;                             \
            p->head = node;                                   \
            node->data = v;                                   \
        } else {                                              \
            __node_ ## n* c = p->head;                        \
            size_t j = 0;                                     \
            if (p->cached.key >= i && p->cached.val) {        \
                c = p->cached.val;                            \
                j = p->cached.key;                            \
            }                                                 \
            for (; j!=i; ++j, c=c->next);                     \
            node->prev = c->prev;                             \
            node->next = c;                                   \
            c->prev->next = node;                             \
            c->prev = node;                                   \
            node->data = v;                                   \
        }                                                     \
        ++*(size_t*)&p->count;                                \
        p->cached.key = i;                                    \
        p->cached.val = node;                                 \
        return 1;                                             \
    }                                                         \
    /*
     * Change the value of a list by index
     *
     * p: list pointer
     * i: index
     * v: a value of type t
     *
     * return: boolean operation success/failure
     */                                                        \
    static int n ## _put(n* p, size_t i, t v) {                \
        if (i >= p->count)                                     \
            return 0;                                          \
        if (p->cached.key == i && p->cached.val) {             \
            p->cached.val->data = v;                           \
            return 1;                                          \
        }                                                      \
        __node_ ## n* c = p->head;                             \
        if (i == p->count-1)                                   \
            c = p->tail;                                       \
        else                                                   \
            for (size_t j=0; j!=i && c->next; ++j, c=c->next); \
        c->data = v;                                           \
        p->cached.key = i;                                     \
        p->cached.val = c;                                     \
        return 1;                                              \
    }                                                          \
    /*
     * Get and remove the last element of a list
     *
     * p: list pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                               \
    static int n ## _pop(n* p, t* v) {                \
        if (!p->tail || !v)                           \
            return 0;                                 \
        __node_ ## n* tofree = p->tail;               \
        *v = p->tail->data;                           \
        if (p->tail->prev) {                          \
            p->tail->prev->next = NULL;               \
            p->tail = p->tail->prev;                  \
        } else {                                      \
            p->head = p->tail = NULL;                 \
        }                                             \
        free(tofree);                                 \
        --*(size_t*)&p->count;                        \
        if (p->count) {                               \
            p->cached.key = p->count-1;               \
            p->cached.val = p->tail;                  \
        } else {                                      \
            memset(&p->cached, 0, sizeof(p->cached)); \
        }                                             \
        return 1;                                     \
    }                                                 \
    /*
     * Get and remove the first element of a list
     *
     * p: vector list
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                               \
    static int n ## _pop_back(n* p, t* v) {           \
        if (!p->count || !v)                          \
            return 0;                                 \
        if (p->get(p, 0, v)) {                        \
            p->remove(p, 0);                          \
            memset(&p->cached, 0, sizeof(p->cached)); \
        }                                             \
        return 1;                                     \
    }                                                 \
    /*
     * Get a list element by index
     *
     * p: list pointer
     * i: index
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                                          \
    static int n ## _get(n* p, size_t i, t* v) {                 \
        if (!p->count || !v || i>=p->count)                      \
            return 0;                                            \
        if (i<p->count && p->cached.key == i && p->cached.val) { \
            *v = p->cached.val->data;                            \
            return 1;                                            \
        }                                                        \
        __node_ ## n* c = p->head;                               \
        size_t j = 0;                                            \
        if (p->cached.key < i && p->cached.val) {                \
            j = p->cached.key;                                   \
            c = p->cached.val;                                   \
        }                                                        \
        for (; j!=i && c; ++j)                                   \
            c = c->next;                                         \
        p->cached.key = i;                                       \
        p->cached.val = c;                                       \
        *v = c->data;                                            \
        return 1;                                                \
    }                                                            \
    /*
     * Get the first element of a list
     *
     * p: list pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                  \
    static int n ## _first(n* p, t* v) { \
        if (!p->count || !v)             \
            return 0;                    \
        *v = p->head->data;              \
        return 1;                        \
    }                                    \
    /*
     * Get the last element of a list
     *
     * p: list pointer
     * v (output): a pointer of type t
     *
     * return: boolean operation success/failure
     */                                 \
    static int n ## _last(n* p, t* v) { \
        if (!p->count || !v)            \
            return 0;                   \
        *v = p->tail->data;             \
        return 1;                       \
    }                                   \
    /*
     * Remove a list element by index
     *
     * p: list pointer
     * i: index
     *
     * return: boolean operation success/failure
     */                                               \
    static int n ## _remove(n* p, size_t i) {         \
        if (!p->count)                                \
            return 1;                                 \
        if (i>=p->count)                              \
            return 0;                                 \
        if (p->cached.key <= i)                       \
            memset(&p->cached, 0, sizeof(p->cached)); \
        __node_ ## n* c = p->head;                    \
        for (size_t j=0; j!=i; ++j)                   \
            c = c->next;                              \
        if ((c == p->head) && (c == p->tail)) {       \
            p->head = p->tail = NULL;                 \
        } else if (c == p->head) {                    \
            c->next->prev = NULL;                     \
            p->head = c->next;                        \
        } else if (c == p->tail) {                    \
            c->prev->next = NULL;                     \
            p->tail = c->prev;                        \
        } else {                                      \
            c->next->prev = c->prev;                  \
            c->prev->next = c->next;                  \
        }                                             \
        free(c);                                      \
        --*(size_t*)&p->count;                        \
        return 1;                                     \
    }                                                 \
    /*
     * Remove all elements from a list
     *
     * p: list pointer
     */                                           \
    static void n ## _clear(n* p) {               \
        if (!p->count)                            \
            return;                               \
        __node_ ## n* i = p->head;                \
        __node_ ## n* j;                          \
        do {                                      \
            j = i->next;                          \
            free(i);                              \
            i = j;                                \
        } while (i);                              \
        *(size_t*)&p->count = 0;                  \
        p->head = p->tail = NULL;                 \
        memset(&p->cached, 0, sizeof(p->cached)); \
    }                                             \
    /*
     * Release list's resources
     *
     * p: list pointer
     */                            \
    static void n ## _free(n* p) { \
        n ## _clear(p);            \
        free(p);                   \
    }                              \
    /*
     * Initialize list's resources
     *
     * p: list pointer
     */                                 \
    n* n ## _init(n* p) {               \
        if (!p)                         \
            return NULL;                \
        memset(p, 0, sizeof(*p));       \
        p->push = n ## _push;           \
        p->push_back = n ## _push_back; \
        p->put = n ## _put;             \
        p->insert = n ## _insert;       \
        p->pop = n ## _pop;             \
        p->pop_back = n ## _pop_back;   \
        p->get = n ## _get;             \
        p->first = n ## _first;         \
        p->last = n ## _last;           \
        p->remove = n ## _remove;       \
        p->clear = n ## _clear;         \
        p->free =  n ## _free;          \
        return p;                       \
    }

/*
 * Declare a pair of specific types.
 *
 * t1: key type
 * t2: value type
 * n: new type name
 */
#define dectype_pair(t1, t2, n)  \
    typedef struct n {           \
        t1 key;                  \
        t2 val;                  \
        void (*free)(struct n*); \
    } n;                         \
    n* n ## _init(struct n*);

#define define_pair(t1, t2, n)            \
    static void n ## _free(struct n* p) { \
        free(p);                          \
    }                                     \
    n* n ## _init(struct n* p) {  \
        if (!p)                   \
            return NULL;          \
        memset(p, 0, sizeof(*p)); \
        p->free = n ## _free;     \
        return p;                 \
    }

/*
 * Initial number of elements of a map
 */
#ifndef MAPSIZE
#define MAPSIZE 47U
#endif

/*
 * Default load factor
 */
#ifndef LFACTOR 
#define LFACTOR 0.75
#endif

/*
 * Declare a map of a specific type.
 * By default pointers/values are hashed,
 * not whole strings; to change this behaviour
 * and use strings (for maps that have strings
 * as keys), set strcmp field to 1:
 * es. p->strcmp = 1;
 *
 * t1: key type
 * t2: value type
 * n: new type name
 */
#define dectype_map(t1, t2, n)                   \
    dectype_pair(t1, t2, __pair_ ## n)           \
    dectype_list(__pair_ ## n*, __bucket_ ## n)  \
    dectype_vector(__pair_ ## n*, __items_ ## n) \
    dectype_vector(__bucket_ ## n*, __vec_ ## n) \
    typedef struct n {                           \
        __vec_ ## n* data;                       \
        __pair_ ## n* cached;                    \
        __bucket_ ## n* r;                       \
        int strcmp;                              \
        size_t count;                            \
        size_t size;                             \
        float lfactor;                           \
        void (*free)(struct n*);                 \
        int (*full)(struct n*);                  \
        int (*rehash)(struct n*);                \
        int (*insert)(struct n*, t1, t2);        \
        __items_ ## n* (*items)(struct n*);      \
        int (*get)(struct n*, t1, t2*);          \
        int (*item)(struct n*, t1, void*);       \
        int (*contains)(struct n*, t1);          \
        int (*remove)(struct n*, t1);            \
        void (*clear)(struct n*);                \
    } n;                                         \
    n* n ## _init(n*);

#define define_map(t1, t2, n)                   \
    define_pair(t1, t2, __pair_ ## n)           \
    define_list(__pair_ ## n*, __bucket_ ## n)  \
    define_vector(__pair_ ## n*, __items_ ## n) \
    define_vector(__bucket_ ## n*, __vec_ ## n) \
    /*
     * Check if hashmap is saturated (have to be
     * rehashed).
     *
     * p: map pointer
     *
     * return: boolean saturated
     */                                            \
    static int n ## _full(n* p) {           \
        return p->count >= (p->size * p->lfactor); \
    }                                              \
    /*
     * Returns a vector with key-value pairs;
     * the vector must be freed after use.
     *
     * p: map pointer
     *
     * return: pointer to vector of pairs on success,
     * NULL on failure.
     */                                               \
    static __items_ ## n* n ## _items(n* p) {         \
        __items_ ## n* b = alloc(__items_ ## n);      \
        __bucket_ ## n* l;                            \
        __pair_ ## n* e;                              \
        size_t r = p->count;                          \
        for (size_t i=0; i<p->data->count && r; ++i)  \
            if (p->data->get(p->data, i, &l))         \
                if (l)                                \
                    for (size_t j=0; j<l->count; ++j) \
                        if (l->get(l, j, &e)) {       \
                            if (b->push(b, e)) {      \
                                --r;                  \
                            } else {                  \
                                release(b);           \
                                return NULL;          \
                            }                         \
                        }                             \
        return b;                                     \
    }                                                 \
    /*
     * Rehash map
     *
     * p: map pointer
     *
     * return: boolean operation success/failure
     */                                                    \
    static int n ## _rehash(n* p) {                 \
        if (p->r || (p->size > (p->size+p->size)))         \
            return 0;                                      \
        p->cached = NULL;                                  \
        __bucket_ ## n* r = alloc(__bucket_ ## n);         \
        p->r = r;                                          \
        __bucket_ ## n* b;                                 \
        __pair_ ## n* e;                                   \
        while (p->data->count) {                           \
            if (p->data->pop(p->data, &b)) {               \
                if (b) {                                   \
                    while (b->count) {                     \
                        if (b->pop(b, &e)) {               \
                            if (!r->push(r, e)) {          \
                                p->data->push(p->data, b); \
                                return 0;                  \
                            }                              \
                        } else {                           \
                            return 0;                      \
                        }                                  \
                    }                                      \
                    b->free(b);                            \
                }                                          \
            } else {                                       \
                return 0;                                  \
            }                                              \
        }                                                  \
        while (p->count >= (p->size*p->lfactor)) {         \
            if (p->size > (p->size+p->size)) {             \
                p->size = ((size_t)-1);                    \
                break;                                     \
            }                                              \
            p->size *= 2;                                  \
        }                                                  \
        *(size_t*)&p->count = 0;                           \
        for (size_t i=0; i<p->size; ++i)                   \
            if (!p->data->push(p->data, NULL))             \
                return 0;                                  \
        while (r->count) {                                 \
            if (r->pop(r, &e)) {                           \
                if (p->insert(p, e->key, e->val)) {        \
                    e->free(e);                            \
                } else {                                   \
                    r->push(r, e);                         \
                    return 0;                              \
                }                                          \
            }                                              \
        }                                                  \
        r->free(r);                                        \
        return 1;                                          \
    }                                                      \
    static size_t __ ## n ## _hash(n* p, t1 k) {                  \
        char b[64] = {0};                                         \
        size_t h;                                                 \
        char* s;                                                  \
        sprintf(b, "%p", (void*)(size_t)k);                       \
        s = p->strcmp ? (char*)(size_t)strtoull(b, NULL, 16) : b; \
        size_t l = strlen(s);                                     \
        h = 3;                                                    \
        for (size_t i=0; i<l; ++i)                                \
            h = (h << 7) + (i ^ s[i]) + l;                        \
        return h % p->size;                                       \
    }                                                             \
    static int __ ## n ## _cmpkeys(n* p, t1 ok, t1 nk) {     \
        if (!ok || !nk)                                      \
            return 0;                                        \
        if (p->strcmp)                                       \
            return eq((char*)(size_t)ok, (char*)(size_t)nk); \
        return ok == nk;                                     \
    }                                                        \
    /*
     * Insert key and value in map
     *
     * p: map pointer
     * k: key of type t1
     * v: value of type t2
     *
     * return: boolean operation success/failure
     */                                                  \
    static int n ## _insert(n* p, t1 k, t2 v) {          \
        if (!(p->count+1))                               \
            return 0;                                    \
        size_t x = __ ## n ## _hash(p, k);               \
        __bucket_ ## n* b;                               \
        __pair_ ## n* e;                                 \
        if (!p->data->get(p->data, x, &b))               \
            return 0;                                    \
        int ja = 0;                                      \
        int je = 0;                                      \
        if (!b) {                                        \
            ja = 1;                                      \
            b = alloc(__bucket_ ## n);                   \
            if (!b)                                      \
                return 0;                                \
            if (!p->data->put(p->data, x, b)) {          \
                b->free(b);                              \
                return 0;                                \
            }                                            \
        }                                                \
        for (size_t i=0; i<b->count; ++i)                \
            if (b->get(b, i, &e))                        \
                if (__ ## n ## _cmpkeys(p, e->key, k)) { \
                    e->key = k;                          \
                    e->val = v;                          \
                    return 1;                            \
                }                                        \
        e = alloc(__pair_ ## n);                         \
        if (!e) {                                        \
            if (ja)                                      \
                b->free(b);                              \
            return 0;                                    \
        }                                                \
        je = 1;                                          \
        e->key = k;                                      \
        e->val = v;                                      \
        if (!b->push(b, e)) {                            \
            if (ja)                                      \
                b->free(b);                              \
            if (je)                                      \
                e->free(e);                              \
            return 0;                                    \
        }                                                \
        ++*(size_t*)&p->count;                           \
        return 1;                                        \
    }                                                    \
    /*
     * Get a value from a key
     *
     * p: map pointer
     * k: key of type t1
     * v (output): pointer value of type t2
     *
     * return: boolean operation success/failure
     */                                                               \
    static int n ## _get(n* p, t1 k, t2* v) {                         \
        if (!p->count || !v)                                          \
            return 0;                                                 \
        if (p->cached && __ ## n ## _cmpkeys(p, p->cached->key, k)) { \
            *v = p->cached->val;                                      \
            return 1;                                                 \
        }                                                             \
        __bucket_ ## n* b;                                            \
        __pair_ ## n* e;                                              \
        size_t x = __ ## n ## _hash(p, k);                            \
        if (!p->data->get(p->data, x, &b))                            \
            return 0;                                                 \
        if (b && b->count) {                                          \
            for (size_t i=0; i<b->count; ++i) {                       \
                if (!b->get(b, i, &e))                                \
                    return 0;                                         \
                if (__ ## n ## _cmpkeys(p, e->key, k)) {              \
                    p->cached = e;                                    \
                    *v = e->val;                                      \
                    return 1;                                         \
                }                                                     \
            }                                                         \
        }                                                             \
        return 0;                                                     \
    }                                                                 \
    /*
     * Get key, value pair from a key
     *
     * p: map pointer
     * k: key of type t1
     * v (output): pair of type <t1, t2>
     *
     * return: boolean operation success/failure
     */                                                               \
    static int n ## _item(n* p, t1 k, void* v) {                      \
        if (!p->count || !v)                                          \
            return 0;                                                 \
        __pair_ ## n** t = (__pair_ ## n**)v;                         \
        if (p->cached && __ ## n ## _cmpkeys(p, p->cached->key, k)) { \
            *t = p->cached;                                           \
            return 1;                                                 \
        }                                                             \
        __bucket_ ## n* b;                                            \
        __pair_ ## n* e;                                              \
        size_t x = __ ## n ## _hash(p, k);                            \
        if (!p->data->get(p->data, x, &b))                            \
            return 0;                                                 \
        if (b && b->count) {                                          \
            for (size_t i=0; i<b->count; ++i) {                       \
                if (!b->get(b, i, &e))                                \
                    return 0;                                         \
                if (__ ## n ## _cmpkeys(p, e->key, k)) {              \
                    p->cached = e;                                    \
                    *t = e;                                           \
                    return 1;                                         \
                }                                                     \
            }                                                         \
        }                                                             \
        return 0;                                                     \
    }                                                                 \
    /*
     * Check if there is a key in a map
     *
     * p: map pointer
     * k: key of type t1
     *
     * return: boolean contains key
     */                                     \
    static int n ## _contains(n* p, t1 k) { \
        t2 tmp;                             \
        return p->get(p, k, &tmp);          \
    }                                       \
    /*
     * Remove key and value by key
     *
     * p: map pointer
     * k: key of type t1
     *
     * return: boolean operation success/failure
     */                                                             \
    static int n ## _remove(n* p, t1 k) {                           \
        if (!p->count)                                              \
            return 0;                                               \
        if (p->cached && __ ## n ## _cmpkeys(p, p->cached->key, k)) \
            p->cached = NULL;                                       \
        size_t x = __ ## n ## _hash(p, k);                          \
        __bucket_ ## n* b;                                          \
        __pair_ ## n* e;                                            \
        if (!p->data->get(p->data, x, &b))                          \
            return 0;                                               \
        if (b && b->count) {                                        \
            for (size_t i=0; i<b->count; ++i) {                     \
                if (!b->get(b, i, &e))                              \
                    return 0;                                       \
                if (__ ## n ## _cmpkeys(p, e->key, k)) {            \
                    b->remove(b, i);                                \
                    e->free(e);                                     \
                    --*(size_t*)&p->count;                          \
                    return 1;                                       \
                }                                                   \
            }                                                       \
        }                                                           \
        return 0;                                                   \
    }                                                               \
    /*
     * Remove all items from a map
     *
     * p: map pointer
     *
     * return: boolean operation success/failure
     */                                     \
    static void n ## _clear(n* p) {         \
        p->cached = NULL;                   \
        __bucket_ ## n* b;                  \
        __pair_ ## n* e;                    \
        while (p->data->count) {            \
            if (!p->data->pop(p->data, &b)) \
                return;                     \
            if (!b)                         \
                continue;                   \
            while (b->count)                \
                if (b->pop(b, &e))          \
                    if (e)                  \
                        e->free(e);         \
            b->free(b);                     \
        }                                   \
    }                                       \
    /*
     * Release map's resources
     *
     * p: map pointer
     */                                   \
    static void n ## _free(n* p) { \
        n ## _clear(p);                   \
        p->data->free(p->data);           \
        free(p);                          \
    }                                     \
    /*
     * Initialize map's resources
     *
     * p: map pointer
     */                                   \
    n* n ## _init(n* p) {                 \
        if (!p)                           \
            return NULL;                  \
        memset(p, 0, sizeof(*p));         \
        p->full = n ## _full;             \
        p->rehash = n ## _rehash;         \
        p->insert = n ## _insert;         \
        p->items = n ## _items;           \
        p->get = n ## _get;               \
        p->item = n ## _item;             \
        p->contains = n ## _contains;     \
        p->remove = n ## _remove;         \
        p->clear = n ## _clear;           \
        p->free =  n ## _free;            \
        p->data = alloc(__vec_ ## n);     \
        if (!p->data)                     \
            return NULL;                  \
        p->size = MAPSIZE;                \
        p->lfactor = LFACTOR;             \
        for (size_t i=0; i<p->size; ++i)  \
            p->data->push(p->data, NULL); \
        return p;                         \
    }

/*
 * Reverse a vector or a list in-place.
 *
 * e: container item's type
 * c: pointer to container
 */
#define vlrev(e, c)             \
    do {                        \
        if (!c || c->count < 2) \
            break;              \
        e l;                    \
        e r;                    \
        size_t i = 0;           \
        size_t j = c->count-1;  \
        while (i<j) {           \
            c->get(c, i, &l);   \
            c->get(c, j, &r);   \
            c->put(c, i, r);    \
            c->put(c, j, l);    \
            ++i;                \
            --j;                \
        }                       \
    } while (0)

/*
 * Swap two items of a vector or a list by index.
 *
 * c: pointer to container
 * e: container item's type
 * i1: first index
 * i2: second index
 */
#define vlswap(c, e, i1, i2)                                      \
    do {                                                          \
        if (!c || i1 > c->count-1 || i2 > c->count-1 || i1 == i2) \
            break;                                                \
        size_t __i1 = i1;                                         \
        size_t __i2 = i2;                                         \
        e e1;                                                     \
        e e2;                                                     \
        c->get(c, __i1, &e1);                                     \
        c->get(c, __i2, &e2);                                     \
        c->put(c, __i1, e2);                                      \
        c->put(c, __i2, e1);                                      \
    } while (0)

/*
 * For each item (pair) of a map
 * do something.
 *
 * m: map pointer
 * t: type of pair
 * p: pointer to pair of contained type
 * c: code to execute
 */
#define eachitem(m, t, p, c)                        \
    do {                                            \
        if (!m)                                     \
            break;                                  \
        vec_ ## t* __v = (vec_ ## t*)m->items(m);   \
        if (!__v)                                   \
            break;                                  \
        for (size_t __i=0; __i<__v->count; ++__i) { \
            if (__v->get(__v, __i, &p)) {           \
                c                                   \
            }                                       \
        }                                           \
        release(__v);                               \
    } while (0)

/*
 * For each element of a vector/list
 * do something.
 *
 * p: list/vector pointer
 * e: pointer to a value of contained type
 * c: code to execute
 */
#define each(p, e, c)                             \
    do {                                          \
        if (!p)                                   \
            break;                                \
        for (size_t __i=0; __i<p->count; ++__i) { \
            if (p->get(p, __i, &e)) {             \
                c                                 \
            }                                     \
        }                                         \
    } while (0)

/*
 * Call a function on a pointer
 * at the end of a block of code
 *
 * p: pointer to a value
 * e: function
 * c: code to execute
 */
#define with(p, f, c) \
    do {              \
        {             \
            c         \
        }             \
        f(p);         \
    } while (0)

/*
 * Splits a string into multiple
 * strings and puts them into
 * an already allocated list or vector.
 * If a delimiter replacer is given (r),
 * and it's not NULL, then append a copy
 * of it to list or vector.s
 *
 * s: string to split
 * d: delimiter
 * p: pointer to a list or a vector
 * r: string to replace delimiter
 */
#define ssplit(s, d, p, r)                                     \
    do {                                                       \
        if (!p)                                                \
            break;                                             \
        char* __sub = NULL;                                    \
        size_t __b = 0;                                        \
        size_t __l = 0;                                        \
        size_t __sl = strlen(s);                               \
        for (size_t __i=0; __i<__sl; ++__i) {                  \
            if (s[__i] == d) {                                 \
                if (__l) {                                     \
                    __sub = calloc(1, sizeof(char)*(__l+1));   \
                    if (!__sub)                                \
                        break;                                 \
                    size_t __j = __b;                          \
                    for (size_t __x=0;  __x<__l; ++__x, ++__j) \
                        __sub[__x] = s[__j];                   \
                    p->push(p, __sub);                         \
                    __l = 0;                                   \
                }                                              \
                if (r)                                         \
                    p->push(p, salloc(r));                     \
            } else {                                           \
                if (!__l)                                      \
                    __b = __i;                                 \
                ++__l;                                         \
                if (__i==__sl-1) {                             \
                    __sub = calloc(1, sizeof(char)*(__l+1));   \
                    if (!__sub)                                \
                        break;                                 \
                    size_t __j = __b;                          \
                    for (size_t __x=0;  __x<__l; ++__x, ++__j) \
                        __sub[__x] = s[__j];                   \
                    p->push(p, __sub);                         \
                    __l = 0;                                   \
                }                                              \
            }                                                  \
        }                                                      \
    } while (0)

/*
 * Join a vector of strings or a list of strings
 * into a single string. If the output buffer (b) is NULL, the
 * memory will be automatically allocated, and must be freed
 * after use; otherwise this macro simply uses the given 
 * allocated buffer.
 *
 * p: string to split
 * s: delimiter string
 * b: string buffer
 */
#define cjoin(p, s, b)                                                 \
    do {                                                               \
        if (!p || !p->count)                                           \
           break;                                                      \
                                                                       \
        size_t __lb = 0;                                               \
        size_t __ls;                                                   \
        size_t __lc;                                                   \
        char* __c;                                                     \
        int __f = 0;                                                   \
                                                                       \
        if (!b) {                                                      \
            b = calloc(1, sizeof(char));                               \
            if (!b)                                                    \
                 break;                                                \
        } else {                                                       \
            __f = 1;                                                   \
            b[0] = 0;                                                  \
        }                                                              \
        for (size_t __i=0; __i<p->count; ++__i) {                      \
            if (p->get(p, __i, &__c) && __c) {                         \
                __lc = strlen(__c);                                    \
                __ls = (__i+1==p->count) ? 0 : strlen(s);              \
                if (!__f) {                                            \
                    char* __m = NULL;                                  \
                    __m = realloc(b, sizeof(char)*(__lb+__lc+__ls+1)); \
                    if (__m) {                                         \
                        b = __m;                                       \
                    } else {                                           \
                        free(b);                                       \
                        b = NULL;                                      \
                    }                                                  \
                }                                                      \
                memcpy(b+__lb, __c, __lc);                             \
                memcpy(b+__lb+__lc, s, __ls);                          \
                __lb += __lc+__ls;                                     \
            }                                                          \
        }                                                              \
        b[__lb] = 0;                                                   \
    } while (0)

/*
 * Check if a vector of strings or a list of strings
 * contains a specified string.
 *
 * p: list/vector of strings
 * v: string to search
 * r: boolean output value
 */
#define scont(p, v, r)        \
    do {                      \
        r = 0;                \
        const char* __s;      \
        each(p, __s,          \
            if (eq(__s, v)) { \
                r = 1;        \
                break;        \
            }                 \
        );                    \
    } while (0)

#define declare_utils()                                               \
    int eq(const char*, const char*);                                 \
    int ieq(const char*, const char*);                                \
    char* sjoin(char*, const char*, ...);                             \
    int oneof(char, const char*);                                     \
    char* shellsub(char*, const char*);                               \
    int cmdret(const char*);                                          \
    char* trim(char*);                                                \
    char* salloc(char*);                                              \
    char* sadd(char*, const char*);                                   \
    char* sins(char*, const char*, size_t);                           \
    char* srep(char*, const char*, const char*, const char*, size_t); \
    int valid(char);                                                  \
    int isfile(int);                                                  \
    int issock(int);                                                  \
    int ispipe(int);                                                  \


#define define_utils()                 \
/*
 * Compare 2 strings for equality
 *
 * a: first string
 * b: second string
 *
 * return: boolean is/not equal
 */                                    \
int eq(const char* a, const char* b) { \
    if (a == b)                        \
        return 1;                      \
    if (!a || !b)                      \
        return 0;                      \
    return !strcmp(a, b);              \
}                                      \
/*
 * Compare 2 strings for equality (case insensitive)
 *
 * a: first string
 * b: second string
 *
 * return: boolean is/not equal
 */                                                                             \
int ieq(const char* a, const char* b) {                                         \
    if (a == b)                                                                 \
        return 1;                                                               \
    if (!a || !b)                                                               \
        return 0;                                                               \
    for (; *a; a++, b++) {                                                      \
        if ((*a < 0x41 || (*a >= 0x5B && *a <= 0x60) || *a > 0x7A) && *a != *b) \
            return 0;                                                           \
        if ((*a & ~(1<<5)) != (*b & ~(1<<5)))                                   \
            return 0;                                                           \
    }                                                                           \
    return 1;                                                                   \
}                                                                               \
/*
 * Joins a variable number of strings into a single
 * one, using a specified separator. If a NULL
 * buffer is given, then the memory is automatically
 * allocated, and must be freed after use; otherwise
 * the given buffer is used. The final argument to this
 * function must be NULL!
 *
 * r: output buffer
 * s: separator string
 * ...: strings to join
 *
 * return: string buffer
 */                                                          \
char* sjoin(char* r, const char* s, ...) {                   \
    if (!s)                                                  \
        return NULL;                                         \
                                                             \
    va_list args;                                            \
    va_start(args, s);                                       \
                                                             \
    const char* a;                                           \
    size_t la;                                               \
                                                             \
    size_t ls = strlen(s);                                   \
    size_t lr = 0;                                           \
    size_t c = 0;                                            \
    char* R = r;                                             \
                                                             \
    do {                                                     \
        if (c>20) {                                          \
            va_end(args);                                    \
            return NULL;                                     \
        }                                                    \
        a = va_arg(args, const char*);                       \
    } while (++c, a);                                        \
    --c;                                                     \
    va_end(args);                                            \
    va_start(args, s);                                       \
    for (size_t i=0; i<c; ++i) {                             \
        const char* a = va_arg(args, const char*);           \
        la = strlen(a);                                      \
        ls = (i+1==c) ? 0 : strlen(s);                       \
        if (!R) {                                            \
            char* m = realloc(r, sizeof(char)*(lr+la+ls+1)); \
            if (!m) {                                        \
                free(r);                                     \
                r = NULL;                                    \
                break;                                       \
            }                                                \
            r = m;                                           \
        }                                                    \
        memcpy(r+lr, a, la);                                 \
        memcpy(r+lr+la, s, ls);                              \
        lr += la + ls;                                       \
    }                                                        \
    va_end(args);                                            \
    r[lr] = 0;                                               \
    return r;                                                \
}                                                            \
/*
 * Checks if a character is present inside
 * a given string.
 *
 * c: character to check
 * s: string to search
 *
 * return: boolean character is inside
 */                                \
int oneof(char c, const char* s) { \
    do {                           \
        if (c == *s)               \
            return 1;              \
    } while (*s++);                \
    return 0;                      \
}                                  \
/*
 * Uses popen to get the output of a shell command
 * and puts it in a given buffer. If the buffer is NULL,
 * the memory is automatically allocated, and must
 * be freed after use.
 *
 * s: output buffer
 * c: command
 *
 * return: string buffer
 */                                             \
char* shellsub(char* s, const char* c) {        \
    FILE* f = popen(c, "r");                    \
    char* r = NULL;                             \
    char* m = NULL;                             \
    size_t n = 0;                               \
    size_t ls = 0;                              \
    size_t l = 0;                               \
                                                \
    int b = !!s;                                \
    int ft = 1;                                 \
                                                \
    if (!f)                                     \
        return NULL;                            \
                                                \
    while (getline(&r, &n, f)>0) {              \
        ls = s ? strlen(s) : 0;                 \
        l = ls + n;                             \
        if (!b) {                               \
            m = realloc(s, sizeof(char)*(l+1)); \
            if (!m) {                           \
                free(r);                        \
                free(s);                        \
                pclose(f);                      \
                return NULL;                    \
            }                                   \
            s = m;                              \
        }                                       \
        if (ft) {                               \
            ft = 0;                             \
            s[0] = 0;                           \
        }                                       \
        strcat(s, r);                           \
    }                                           \
    pclose(f);                                  \
    free(r);                                    \
    return s;                                   \
}                                               \
/*
 * Executes a given command and returns
 * its exit status.
 *
 * s: command
 *
 * return: command exit status
 */                                           \
int cmdret(const char* s) {                   \
    int r = system(s);                        \
    return WIFEXITED(r) ? WEXITSTATUS(r) : r; \
}                                             \
/*
 * Removes the trailing newline from a string.
 *
 * s: string
 *
 * return: string buffer
 */                       \
char* trim(char* s) {     \
    if (!s)               \
        return NULL;      \
                          \
    size_t l = strlen(s); \
    if (s[l-1] == '\n')   \
        s[l-1] = 0;       \
    return s;             \
}                         \
/*
 * Allocates enough memory to copy
 * a given string, and then copies it.
 * The string returned must be freed after use.
 *
 * s: string
 *
 * return: allocated string buffer
 */                                \
char* salloc(char* s) {            \
    if (!s)                        \
        return NULL;               \
                                   \
    char* r;                       \
    size_t l = strlen(s);          \
    r = calloc(l+1, sizeof(char)); \
    if (!r)                        \
        return NULL;               \
    strcpy(r, s);                  \
    return r;                      \
}                                  \
/*
 * Inserts a string into another one at specific
 * position and returns the result. If initial string is NULL,
 * memory is automatically allocated, and must
 * be freed after use, otherwise memory is reallocated
 * to insert the string.
 * Never pass a buffer to this function!
 *
 * s: first string
 * a: second string
 *
 * return: (re)allocated string buffer
 */                                            \
char* sins(char* s, const char* a, size_t p) { \
    if (!a || !*a)                             \
        return s ? s : NULL;                   \
                                               \
    size_t ls = 0;                             \
    size_t la = strlen(a);                     \
    char* m;                                   \
                                               \
    if (s)                                     \
        ls = strlen(s);                        \
    if (p > ls)                                \
        return NULL;                           \
    m = realloc(s, sizeof(char)*(ls+la+1));    \
    if (!m)                                    \
        return NULL;                           \
    s = m;                                     \
    if (!ls)                                   \
        s[0] = 0;                              \
                                               \
    size_t i = ls+la;                          \
    size_t j = ls;                             \
    for (; j>=p; --i, --j) {                   \
        s[i] = s[j];                           \
        if (!j)                                \
            break;                             \
    }                                          \
    i = 0;                                     \
    while (i < la) {                           \
        s[p+i] = a[i];                         \
        ++i;                                   \
    }                                          \
    return s;                                  \
}                                              \
/*
 * Joins two strings into one, and returns
 * the result. If initial string is NULL,
 * memory is automatically allocated, and must
 * be freed after use, otherwise memory is reallocated
 * to add a new string.
 * Never pass a buffer to this function!
 *
 * s: first string
 * a: second string
 *
 * return: (re)allocated string buffer
 */                                         \
char* sadd(char* s, const char* a) {        \
    if (!a)                                 \
        return s ? s : NULL;                \
                                            \
    size_t ls = 0;                          \
    size_t la = strlen(a);                  \
    char* m;                                \
                                            \
    if (s)                                  \
        ls = strlen(s);                     \
    m = realloc(s, sizeof(char)*(ls+la+1)); \
    if (!m)                                 \
        return NULL;                        \
    s = m;                                  \
    if (!ls)                                \
        s[0] = 0;                           \
                                            \
    strcat(s, a);                           \
    return s;                               \
}                                           \
/*
 * Replaces a string found in another string with another
 * string (a given number of times). If output buffer is
 * given, then simply uses it, otherwise memory is automatically
 * allocated, and must be freed after use.
 * If number of replacement is '0', the replaces all occurrences.
 *
 * b: output buffer
 * s: string to search into
 * r: substring to find and replace
 * t: substring replacement
 * n: number of replacements
 *
 * return: string buffer
 */                                                                          \
char* srep(char* b, const char* s, const char* r, const char* t, size_t n) { \
    const char* d = s;                                                       \
    char* B = b;                                                             \
    char* p = NULL;                                                          \
    char* f;                                                                 \
    char* m;                                                                 \
                                                                             \
    size_t ofs;                                                              \
    size_t ls = strlen(s);                                                   \
    size_t lr = strlen(r);                                                   \
    size_t lt = strlen(t);                                                   \
    size_t lret = 0;                                                         \
                                                                             \
    for (size_t o=1; (f = strstr(d, r)); ++o) {                              \
        lret += (f-d) + lt;                                                  \
        if (!B) {                                                            \
            m = realloc(b, sizeof(char)*(lret+1));                           \
            if (!m) {                                                        \
                free(b);                                                     \
                return NULL;                                                 \
            }                                                                \
            b = m;                                                           \
        }                                                                    \
        if (o == 1)                                                          \
            b[0] = 0;                                                        \
                                                                             \
        strncat(b, d, f-d);                                                  \
        strcat(b, t);                                                        \
        d = f+lr;                                                            \
        p = f;                                                               \
        if (n && o == n)                                                     \
            break;                                                           \
    }                                                                        \
    if (!b) {                                                                \
        b = malloc(sizeof(char)*(ls+1));                                     \
        if (!b)                                                              \
            return NULL;                                                     \
        strncpy(b, s, ls+1);                                                 \
        return b;                                                            \
    }                                                                        \
    lret = strlen(b);                                                        \
    if (!B && p) {                                                           \
        m = realloc(b, sizeof(char)*(lret+(lt+ls-(p-s))+1));                 \
        if (!m) {                                                            \
            free(b);                                                         \
            return NULL;                                                     \
        }                                                                    \
        b = m;                                                               \
    }                                                                        \
    ofs = (p - s) + lr;                                                      \
    strncat(b, s+ofs, ls-ofs);                                               \
    return b;                                                                \
}                                                                            \
/*
 * Checks if a character is not "strange" :D
 * 
 * c: character to check
 *
 * return: boolean is not strange
 */                                                             \
int valid(char c) {                                             \
    return (c >= 0x09 && c <=0x0d) || (c >= 0x20 && c <= 0x7e); \
}                                                               \
/*
 * Checks if file descriptor refers to a regular file.
 * Doesn't check for syscall errors.
 * 
 * fd: file descriptor
 *
 * return: boolean is a file
 */                                          \
int isfile(int fd) {                         \
    struct stat sb;                          \
    if (fstat(fd, &sb) == -1)                \
        return 0;                            \
                                             \
    return S_IFREG == (sb.st_mode & S_IFMT); \
}                                            \
/*
 * Checks if file descriptor refers to a socket.
 * Doesn't check for syscall errors.
 * 
 * fd: file descriptor
 *
 * return: boolean is a socket
 */                                           \
int issock(int fd) {                          \
    struct stat sb;                           \
    if (fstat(fd, &sb) == -1)                 \
        return 0;                             \
                                              \
    return S_IFSOCK == (sb.st_mode & S_IFMT); \
}                                             \
/*
 * Checks if file descriptor refers to a pipe.
 * Doesn't check for syscall errors.
 * 
 * fd: file descriptor
 *
 * return: boolean is a pipe
 */                                          \
int ispipe(int fd) {                         \
    struct stat sb;                          \
    if (fstat(fd, &sb) == -1)                \
        return 0;                            \
                                             \
    return S_IFIFO == (sb.st_mode & S_IFMT); \
}                                            \

#endif
