/************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

// cutils usage examples

#include <stdio.h>
#include "cutils.h"

declare_utils()
define_utils()

dectype_vector(int, vector_int_t)
dectype_vector(char*, vector_str_t)

dectype_pair(int, char*, pair_intstr_t)

dectype_list(int, list_int_t)
dectype_vector(pair_intstr_t*, vec_pair_intstr_t)

dectype_map(int, char*, map_intstr_t)
dectype_map(char*, char*, map_strstr_t)

define_vector(int, vector_int_t)
define_vector(char*, vector_str_t)

define_pair(int, char*, pair_intstr_t)

define_list(int, list_int_t)
define_vector(pair_intstr_t*, vec_pair_intstr_t)

define_map(int, char*, map_intstr_t)
define_map(char*, char*, map_strstr_t)

int main() {
    vector_int_t* v = alloc(vector_int_t);
    vector_str_t* s = alloc(vector_str_t);
    
    with(s, release,
        s->push(s, "roby");
        s->push(s, "cassandra");
        s->push(s, "borgo");
        s->push(s, "ACAMPORA");

        char* ret = NULL;
        cjoin(s, "---", ret);
        printf("s concatenation: %s\n", ret);
    );

    int ret;
    v->push(v, 7);
    v->push(v, 71);
    v->push(v, 712);

    each(v, ret,
        printf("v element: %d\n", ret);
    );
    __(v, pop_back, &ret);
    printf("v element that was at the beginning: %d\n", ret);
    v->push_back(v, 33333);
    
    each(v, ret,
        printf("v element: %d\n", ret);
    );
    printf("v count: %lu\n", v->count);


    list_int_t* l = alloc(list_int_t);
    l->push(l, 12);
    l->push(l, 11);
    l->push(l, 8);
    l->push(l, 81);
    l->push(l, 82);
    l->push(l, 833);

    each(l, ret,
        printf("l element: %d\n", ret);
    );
    __(l, pop_back, &ret);
    printf("l element that was at the beginning: %d\n", ret);
    l->push_back(l, 33333);
    each(l, ret,
        printf("l element: %d\n", ret);
    );

    char* string = "hehehey, good news, lets'eat!";
    vector_str_t* y = alloc(vector_str_t);
    
    ssplit(string, ' ', y, NULL);

    char* e;
    each(y, e,
        printf("y element: '%s'\n", e);
        free(e);
    );
    release(y);
    
    printf("l count: %lu\n", l->count);
    l->get(l, 3, &ret);
    printf("l element at 3: %d\n", ret);
    l->pop(l, &ret);
    printf("l popped element: %d\n", ret);
    printf("l count: %lu\n", l->count);
    l->clear(l);

    l->push(l, 12);
    l->push(l, 11);
    l->push(l, 8);
    l->push(l, 81);
    l->push(l, 82);
    l->push(l, 833);

    l->put(l, 2, 8888);
    
    for (size_t i=0; i<l->count; ++i) {
        l->get(l, i, &ret);
        printf("l element: %d\n", ret);
    }
    l->remove(l, 3);
    printf("l count: %lu\n", l->count);
    for (size_t i=0; i<l->count; ++i) {
        l->get(l, i, &ret);
        printf("l element: %d\n", ret);
    }
    l->insert(l, 4, 0);
    l->push(l, -1);
    l->push(l, -3);
    printf("l count: %lu\n", l->count);
    for (size_t i=0; i<l->count; ++i) {
        l->get(l, i, &ret);
        printf("l element: %d\n", ret);
    }
    l->clear(l);

    v->push(v, 7);
    v->push(v, 5);
    v->push(v, 5);
    v->push(v, 5);
    v->pop(v, &ret);
    v->pop(v, &ret);
    v->push(v, 3);
    v->push(v, 2);
    v->push(v, 1);
    v->push(v, 11111);

    v->remove(v, 2);
    v->remove(v, 2);
    v->push(v, 1234);
    v->push(v, 234);
    v->push(v, 34);
    v->push(v, 4);
    v->insert(v, 1, 90);
    v->insert(v, 4, 7);
    printf("v count: %lu\n", v->count);
    for (size_t i=0; i<v->count; ++i) {
        v->get(v, i, &ret);
        printf("v element: %d\n", ret);
    }
    v->clear(v);
    v->push(v, 7);
    printf("v count: %lu\n", v->count);
    for (size_t i=0; i<v->count; ++i) {
        v->get(v, i, &ret);
        printf("vector element: %d\n", ret);
    }
    v->pop(v, &ret);
    v->pop(v, &ret);



    map_intstr_t* m = alloc(map_intstr_t);
    map_strstr_t* ms = alloc(map_strstr_t);
    if (!m || !ms)
        return 1;

    m->insert(m, 1, "foo");
    m->insert(m, 2, "foo+");
    m->insert(m, 200, "bar");
    
    printf("m count: %lu\n", m->count);
    
    vec_pair_intstr_t* x = (vec_pair_intstr_t*)m->items(m);
    printf("m items: %lu\n", x->count);
    for (size_t i=0; i<x->count; ++i) {
        pair_intstr_t* p;
        x->get(x, i, &p);
        printf("%d->%s\n", p->key, p->val);
    }
    release(x);

    ms->insert(ms, "strato", "better");
    ms->insert(ms, "strato", "caster");
    ms->insert(ms, "lol", "but moves");
    
    printf("ms count: %lu\n", ms->count);
    printf("ms contains 'strato'?: %d\n", ms->contains(ms, "strato"));
    
    char* r;
    ms->get(ms, "strato", &r);
    printf("ms get value with key 'strato': %s\n", r);

    printf("m count before 4000-elements insertion: %lu\n", m->count);
    for (size_t i=4; i<4005; ++i)
        m->insert(m, i, "test");
    printf("m count after 4000-elements insertion: %lu\n", m->count);
    printf("m size: %lu\n", m->size);
    
    if (m->full(m)) {
        puts("m is saturated!");
        puts("rehashing ...");
        m->rehash(m);
    }
    if (m->full(m))
        puts("m is still saturated!");
    printf("m size after rehash: %lu\n", m->size);
    printf("m count after rehash: %lu\n", m->count);

    m->get(m, 7, &r);

    m->remove(m, 200);
    m->remove(m, 200);
    m->remove(m, 200);
    m->remove(m, 765);
    m->remove(m, 1);
    m->remove(m, 2);
    m->remove(m, 2);
    m->remove(m, 2);
    m->remove(m, 2);
    printf("m count: %lu\n", m->count);
    m->clear(m);
    printf("m count: %lu\n", m->count);

    v->pop(v, &ret);
    v->pop(v, &ret);
    v->pop(v, &ret);
    v->pop(v, &ret);
    v->pop(v, &ret);
    v->clear(v);
    
    ms->clear(ms);

    release(v);
    release(l);
    release(m);
    release(ms);
    
    vector_str_t c;
    vector_str_t o;

    vector_str_t_init(&c);
    c.push(&c, "asdfasdf");
    c.push(&c, "0000asdfasdf");
    printf("static vector c count: %lu\n", c.count);
    
    memcpy(&o, &c, sizeof(o));
    
    printf("static vector o count: %lu\n", o.count);
    
    char* q;
    vector_str_t* po = &o;
    each(po, q, 
        printf("static vector o content: %s\n", q);
    );
    o.clear(&o);
    
    char s1[] = "asdf";
    char s2[] = "asdf";

    map_strstr_t* g = alloc(map_strstr_t);
    g->insert(g, s1, "a");
    g->insert(g, s2, "b");

    printf("g count: %lu\n", g->count);

    // by default values/pointers are directly compared,
    // but if 'strcmp' member field is true, string
    // comparison is used
    map_strstr_t* f = alloc(map_strstr_t);
    f->strcmp = 1;
    f->insert(f, s1, "a");
    f->insert(f, s2, "b");

    printf("f count: %lu\n", f->count);

    release(g);
    release(f);

    char* a = NULL;
    char* b = " second";
    char* S;
    char* T;

    S = sjoin(NULL, "", b, "resto", NULL);
    printf("S is: %s len: %lu\n", S, strlen(S));
    
    T = salloc(S);
    printf("S copy: %s\n", T);
    free(S);
    S = sjoin(NULL, T, " , is here!", "jojjo", NULL);
    printf("joined string: %s\n", S);
    free(S);
    free(T);
    
    S = sjoin(NULL, "-", "a", "b", "ff", "aa", NULL);
    printf("joined string: %s\n", S);
    free(S);

    char bf[256];
    sjoin(bf, "---", "ecco", "qua", NULL);
    printf("joined string: %s\n", bf);

    a = shellsub(NULL, "ls -l");
    printf("'ls -l' output: %s\n", a);
    free(a);

    shellsub(bf, "ls -l");
    printf("'ls -l' output (static buffer): %s\n", bf);

    a = salloc("hey!\nThis is strange.\n");
    printf("string a before trim: |%s|\n", a);
    a = trim(a);
    printf("string a after trim: |%s|\n", a);
    free(a);


    char* sub = "hey lol, have a nice day, lol! Bye";
    printf("string before substitution: %s\n", sub);

    char* rval = srep(NULL, sub, "lol", "X", 0);
    printf("string after substitution: %s\n", rval);
    free(rval);

    rval = srep(NULL, sub, "lol", "", 0);
    printf("string after substitution: %s\n", rval);
    free(rval);

    rval = srep(NULL, sub, "lol", "", 1);
    printf("string after substitution: %s\n", rval);
    free(rval);

    rval = srep(NULL, sub, "lol", "ahahahah", 1);
    printf("string after substitution: %s\n", rval);
    free(rval);

    rval = srep(NULL, sub, "lol", "ahahahah", 0);
    printf("string after substitution: %s\n", rval);
    free(rval);

    char buf[256];

    rval = srep(buf, sub, "lol", "ahahahah", 0);
    printf("string after substitution: %s\n", rval);

    rval = srep(buf, sub, "lol", "ahahahah", 1);
    printf("string after substitution: %s\n", rval);

    rval = srep(buf, sub, "lol", "", 0);
    printf("string after substitution: %s\n", rval);

    rval = srep(buf, sub, "lol", "", 1);
    printf("string after substitution: %s\n", rval);
    
    char* mm = sadd(NULL, "hey, ");
    mm = sadd(mm, "but ");
    mm = sadd(mm, "is it true?");

    printf("sadded string: %s\n", mm);
    free(mm);
    
    return 0;
}
