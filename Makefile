#######################################################################
# Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
########################################################################

CFLAGS = -ggdb -pedantic -Wall -Wextra -Werror

ex: ex.o

ex.o: ex.c cutils.h

debug: ex
	gdb -batch -ex r --args ./ex

memtest:
	valgrind -s -v --track-origins=yes --leak-check=full ./ex

clean:
	rm -f ex ex.o
